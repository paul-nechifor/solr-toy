# Solr Toy

Simple usage of Solr inside of Tomcat.

The Vagrant file uses a slightly modified version of CentOS 6.6, but you can
replace the box with `'chef/centos-6.6'`.

## Usage

Download the Jeopardy data in `private/jeopardy.json`:

    ./scripts/get_data.sh

## License

MIT
